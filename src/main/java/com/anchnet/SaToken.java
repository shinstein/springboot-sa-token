package com.anchnet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SaToken {
    public static void main(String[] args) {
        SpringApplication.run(SaToken.class);
    }
}
