package com.anchnet.permission;

import cn.dev33.satoken.stp.StpInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义权限验证接口扩展
 */
@Component
@Slf4j
public class StpInterfaceImpl implements StpInterface {
    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        // 任意类型的loginId 在登录后，调用 StpUtil.getLoginId() 方法返回的都是 String 类型,故在此处使用 String 类型进行强转
        String id = (String) loginId;

        // 实际项目中，将用户对应的权限存入数据库，然后在该方法中去查询数据库
        ArrayList<String> list = new ArrayList<>();
        if (id.equals("1001")) {
            list.add("user.add");
            list.add("user.delete");
            list.add("user.update");
            list.add("user.view");
        } else {
            list.add("user.view");
        }

        return list;
    }

    /**
     * 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        // 任意类型的loginId 在登录后，调用 StpUtil.getLoginId() 方法返回的都是 String 类型,故在此处使用 String 类型进行强转
        String id = (String) loginId;

        // 实际项目中，将用户对应的角色存入数据库，然后在该方法中去查询数据库
        ArrayList<String> list = new ArrayList<>();
        if (id.equals("1001")) {
            list.add("admin");
            list.add("user");
            list.add("manager");
            list.add("developer");
        } else {
            list.add("user");
            list.add("developer");
        }

        return list;
    }
}
