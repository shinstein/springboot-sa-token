package com.anchnet.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.annotation.SaMode;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import com.anchnet.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
@Slf4j
public class TestController {
    @GetMapping("/login")
    public Result login(Integer userId, String pwd) {
        if ("123".equals(pwd)) {
            // 账号id，建议的类型：（long | int | String）,任意类型的loginId 在登录后，调用 StpUtil.getLoginId() 方法返回的都是 String 类型
            StpUtil.login(userId);

            return Result.success("登录成功");
        } else {
            return Result.fail(HttpStatus.OK.value(), "登录失败");
        }
    }

    @GetMapping("/infos")
    public Result getTokenInfos() {
        // 获取当前会话账号id, 如果未登录，则抛出异常：`NotLoginException`
//        Object loginId = StpUtil.getLoginId();
//        log.info("loginId = {}", loginId);

        // 获取当前会话账号id, 如果未登录，则返回默认值 （`defaultValue`可以为任意类型）
        String defaultLoginId = StpUtil.getLoginId("1");
        log.info("defaultLoginId = {}", defaultLoginId);        // 未登录时defaultLoginId = 1

        // 获取当前会话账号id, 如果未登录，则返回null
        Object loginIdDefaultNull = StpUtil.getLoginIdDefaultNull();
        log.info("getLoginIdDefaultNull = {}", loginIdDefaultNull);     // 未登录时 getLoginIdDefaultNull = null

        SaTokenInfo info = StpUtil.getTokenInfo();
        log.info("SaTokenInfo = {}", info);     // 未登录时SaTokenInfo = SaTokenInfo [tokenName=satoken, tokenValue=null, isLogin=false, loginId=null, loginType=login, tokenTimeout=-2, sessionTimeout=-2, tokenSessionTimeout=-2, tokenActivityTimeout=-2, loginDevice=null, tag=null]
        return Result.success(info);
    }

    @GetMapping("/check-login")
    public Result checkLogin() {
        // 获取当前会话是否已经登录，返回true=已登录，false=未登录
        boolean login = StpUtil.isLogin();
        return login ? Result.success("已登录"):Result.success("未登录");
    }

    /**
     *  sa-token 的注解要生效必须 手动将 Sa-Token 的全局拦截器注册到你项目中,见 SaTokenConfigure
     *
     *  加了 @SaCheckLogin 的方法，在未登录时抛出 cn.dev33.satoken.exception.NotLoginException 异常
     */
    @GetMapping("/check-login1")
    @SaCheckLogin
    public Result checkLogin1() {
        return Result.success("已登录");
    }

    /**
     *  查询当前登录用户角色、权限信息
     */
    @GetMapping("/permission/list/login")
    @SaCheckLogin
    public Result getLoginUserPermissionList() {
        // 查询当前登录用户角色信息
        List<String> roleList = StpUtil.getRoleList();

        // 查询当前登录用户权限信息
        List<String> permissionList = StpUtil.getPermissionList();

        HashMap<String, Object> map = new HashMap<>();
        map.put("roleList", roleList);
        map.put("permissionList", permissionList);
        return Result.success(map);
    }

    /**
     *  查询未登录的用户角色、权限信息
     */
    @GetMapping("/permission/list/nologin")
    public Result getPermissionList(String userId) {
        // 查询指定用户角色信息
        List<String> roleList = StpUtil.getRoleList(userId);

        // 查询指定用户权限信息
        List<String> permissionList = StpUtil.getPermissionList(userId);

        HashMap<String, Object> map = new HashMap<>();
        map.put("roleList", roleList);
        map.put("permissionList", permissionList);
        return Result.success(map);
    }

    /**
     *  测试当前用户权限
     */
    @GetMapping("/permission/test")
    @SaCheckLogin
    public Result testPermission() {
        // 当前账号是否含有指定权限, 返回 true 或 false
        log.info("StpUtil.hasPermission = {}", StpUtil.hasPermission("user.add"));

        // 当前账号是否含有指定权限, 如果验证未通过，则抛出异常: NotPermissionException
        StpUtil.checkPermission("user.add");


        // 当前账号是否含有指定权限 [指定多个，必须全部验证通过]
        StpUtil.checkPermissionAnd("user.add", "user.delete");

        // 当前账号是否含有指定权限 [指定多个，只要其一验证通过即可]
        StpUtil.checkPermissionOr("user.add", "user.delete");


        // 当前账号是否拥有指定角色, 返回 true 或 false
        log.info("StpUtil.hasRole = {}", StpUtil.hasRole("admin"));

        // 当前账号是否含有指定角色标识, 如果验证未通过，则抛出异常: NotRoleException
        StpUtil.checkRole("admin");

        // 当前账号是否含有指定角色标识 [指定多个，必须全部验证通过]
        StpUtil.checkRoleAnd("admin", "user");

        // 当前账号是否含有指定角色标识 [指定多个，只要其一验证通过即可]
        StpUtil.checkRoleOr("admin", "user");

        return Result.success();
    }

    /**
     *  当前接口只有拥有 user.add 的权限的用户才可以访问
     */
    @GetMapping("/permission/check1")
    @SaCheckPermission("user.add")
    public Result checkPermission1() {
        return Result.success("当前用户拥有 user.add 权限");
    }

    /**
     *  当前接口只有同时拥有 user.add 和 user.update 的权限的用户才可以访问
     */
    @GetMapping("/permission/check2")
    @SaCheckPermission(value = {"user.add", "user.update"}, mode = SaMode.AND)
    public Result checkPermission2() {
        return Result.success("当前用户同时拥有 user.add 和 user.update 权限");
    }

    /**
     *  当前接口只有同时拥有 user.add 和 user.update 的权限 或 manager 或 admin 的角色 的用户才可以访问
     *
     *  orRole 字段代表权限认证未通过时的次要选择，两者只要其一认证成功即可通过校验，其有三种写法：
     *
     *  1. orRole = "admin"，代表需要拥有角色 admin 。
     *  2. orRole = {"admin", "manager", "staff"}，代表具有三个角色其一即可。
     *  3. orRole = {"admin, manager, staff"}，代表必须同时具有三个角色。
     */
    @GetMapping("/permission/check3")
    @SaCheckPermission(value = {"user.add", "user.update"}, mode = SaMode.AND, orRole = {"manager", "admin"})
    public Result checkPermission3() {
        return Result.success("当前用户同时拥有 user.add 和 user.update 的权限 或 manager 或 admin 的角色");
    }

    /**
     *  当前接口只有 admin 角色才可以访问
     */
    @GetMapping("/role/check1")
    @SaCheckRole("admin")
    public Result checkRole1(){
        return Result.success("当前用户是 admin 角色");
    }

    /**
     *  当前接口只有 同时是 admin 和  manager 的角色才可以访问
     */
    @GetMapping("/role/check2")
    @SaCheckRole(value = {"admin", "manager"}, mode = SaMode.AND)
    public Result checkRole2(){
        return Result.success("当前用户是 admin 和 manager 角色");
    }

    /**
     *  当前接口只有 同时是 admin 或  manager 的角色才可以访问
     */
    @GetMapping("/role/check3")
    @SaCheckRole(value = {"admin, manager"}, mode = SaMode.OR)
    public Result checkRole3(){
        return Result.success("当前用户是 admin 角色");
    }
}
