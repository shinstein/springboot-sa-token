package com.anchnet.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result implements Serializable {
    private Integer code;

    private String message;

    private Object data;

    public static Result success() {
        Result result = new Result();
        result.setCode(HttpStatus.OK.value());
        return result;
    }

    public static Result success(String msg) {
        Result result = new Result();
        result.setCode(HttpStatus.OK.value());
        result.setMessage(msg);
        return result;
    }

    public static Result success(Object data) {
        Result result = new Result();
        result.setCode(HttpStatus.OK.value());
        result.setData(data);
        return result;
    }

    public static Result fail(Integer code, String message) {
        Result result = new Result();
        result.setCode(code);
        result.setMessage(message);
        return result;
    }
}
