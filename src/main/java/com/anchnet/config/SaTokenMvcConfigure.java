package com.anchnet.config;

import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.router.SaHttpMethod;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import com.anchnet.result.Result;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 *  参考：https://sa-token.cc/doc.html#/use/route-check
 */
@Configuration
public class SaTokenMvcConfigure implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册 Sa-Token 拦截器，打开注解式鉴权功能
        registry.addInterceptor(new SaInterceptor()).addPathPatterns("/**");


        /**
         *  校验函数详解
         */
        // 配置所有请求都必须登录 StpUtil.checkLogin(),排除登录请求
//        registry.addInterceptor(new SaInterceptor(handle -> StpUtil.checkLogin())).addPathPatterns("/**").excludePathPatterns("/login");

        // 配置更多其他请求和校验规则
//        registry.addInterceptor(new SaInterceptor(handle -> {
//            SaRouter.match("/**")   // 拦截的 path 列表，可以写多个 */
//                    .notMatch("/login")         // 排除掉的 path 列表，可以写多个
//                    .check(r -> StpUtil.checkLogin());  // 要执行的校验动作，可以写完整的 lambda 表达式
//
//            // 根据路由划分模块，不同模块不同鉴权
//            SaRouter.match("/user/**", r -> StpUtil.checkPermission("user"));
//            SaRouter.match("/admin/**", r -> StpUtil.checkPermission("admin"));
//            SaRouter.match("/goods/**", r -> StpUtil.checkPermission("goods"));
//            SaRouter.match("/orders/**", r -> StpUtil.checkPermission("orders"));
//            SaRouter.match("/notice/**", r -> StpUtil.checkPermission("notice"));
//            SaRouter.match("/comment/**", r -> StpUtil.checkPermission("comment"));
//
//            // 检查角色
//            SaRouter.match("/admin/**", r -> StpUtil.checkRole("admin"));
//
//            // 甚至你可以随意的写一个打印语句
//            SaRouter.match("/**", r -> System.out.println("----啦啦啦----"));
//
//            // 基础写法样例：匹配一个path，执行一个校验函数
//            SaRouter.match("/user/**").check(r -> StpUtil.checkLogin());
//
//            // 根据 path 路由匹配   ——— 支持写多个path，支持写 restful 风格路由
//            // 功能说明: 使用 /user , /goods 或者 /art/get 开头的任意路由都将进入 check 方法
//            SaRouter.match("/user/**", "/goods/**", "/art/get/{id}").check( /* 要执行的校验函数 */ );
//
//            // 根据 path 路由排除匹配
//            // 功能说明: 使用 .html , .css 或者 .js 结尾的任意路由都将跳过, 不会进入 check 方法
//            SaRouter.match("/**").notMatch("*.html", "*.css", "*.js").check( /* 要执行的校验函数 */ );
//
//            // 根据请求类型匹配
//            SaRouter.match(SaHttpMethod.GET).check( /* 要执行的校验函数 */ );
//
//            // 根据一个 boolean 条件进行匹配
//            SaRouter.match( StpUtil.isLogin() ).check( /* 要执行的校验函数 */ );
//
//            // 根据一个返回 boolean 结果的lambda表达式匹配
//            SaRouter.match( r -> StpUtil.isLogin() ).check( /* 要执行的校验函数 */ );
//
//            // 多个条件一起使用
//            // 功能说明: 必须是 Get 请求 并且 请求路径以 `/user/` 开头
//            SaRouter.match(SaHttpMethod.GET).match("/user/**").check( /* 要执行的校验函数 */ );
//
//            // 可以无限连缀下去
//            // 功能说明: 同时满足 Get 方式请求, 且路由以 /admin 开头, 路由中间带有 /send/ 字符串, 路由结尾不能是 .js 和 .css
//            SaRouter
//                    .match(SaHttpMethod.GET)
//                    .match("/admin/**")
//                    .match("/**/send/**")
//                    .notMatch("/**/*.js")
//                    .notMatch("/**/*.css")
//                    // ....
//                    .check( /* 只有上述所有条件都匹配成功，才会执行最后的check校验函数 */ );
//
//        })).addPathPatterns("/**");

        /**
         *  提前退出所有匹配链
         *
         *  stop():代码运行至第3条匹配链时，会在stop函数处提前退出整个匹配函数，从而忽略掉剩余的所有match匹配
         *  back():停止匹配，结束执行，直接向前端返回结果
         *
         *  SaRouter.stop() 会停止匹配，进入Controller。
         *  SaRouter.back() 会停止匹配，直接返回结果到前端。
         */
//        registry.addInterceptor(new SaInterceptor(r -> {
//            SaRouter.match("/**").check(f -> System.out.println("校验条件1"));
//            SaRouter.match("/**").check(f -> System.out.println("校验条件2"));
//            SaRouter.match("/**").check(f -> System.out.println("校验条件3")).stop();
//            SaRouter.match("/**").check(f -> System.out.println("校验条件4"));
//
//            SaRouter.match("/**").check(f -> System.out.println("校验条件5")).back();
//            SaRouter.match("/**").check(f -> System.out.println("校验条件6")).back(Result.fail(1, "请求失败"));
//        })).addPathPatterns("/**");


        /**
         *  提前退出部分匹配链
         *  free():打开一个独立的作用域，使内部的 stop() 不再一次性跳出整个 Auth 函数，而是仅仅跳出当前 free 作用域。
         *
         *  执行 stop() 函数跳出 free 后继续执行下面的 match 匹配
         */
//        registry.addInterceptor(new SaInterceptor(handle -> {
//            SaRouter.match("/**").free(r -> {
//                SaRouter.match("/a/**").check(f -> System.out.println("check1"));
//                SaRouter.match("/b/**").check(f -> System.out.println("check2")).stop();
//                SaRouter.match("/c/**").check(f -> System.out.println("check3"));
//            });
//
//            SaRouter.match("/**").check(r -> System.out.println("check4"));
//        })).addPathPatterns("/**");
    }
}
