package com.anchnet.config;

import cn.dev33.satoken.config.SaTokenConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *  sa-token 配置，代码配置优先级高于配置文件配置
 */
@Configuration
public class SaTokenConfigure {
    @Bean
    public SaTokenConfig saTokenConfig() {
        SaTokenConfig config = new SaTokenConfig();

        config.setTokenName("sa-token");

        // token的长久有效期(单位:秒) 默认30天, -1代表永久
        config.setTimeout(3600);

        // token临时有效期 [指定时间内无操作就视为token过期] (单位: 秒), 默认-1 代表不限制
        config.setActivityTimeout(600);

        // 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录)
        config.setIsConcurrent(true);

        // token风格(默认可取值：uuid、simple-uuid、random-32、random-64、random-128、tik)
        config.setTokenStyle("uuid");

        return config;
    }
}
